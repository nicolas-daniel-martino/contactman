## Klee formation project

## Project Objectives
Build a CLI app to manage simple contacts.

Build with java 8. Written in eclipse.

## How to Run with eclipse
Clone the directory first...
In a new workspace: chose file > import > general > Existing project into Workspace > next > browse > select directory contactman > finish

To launch: right click on src and run as Java app.

## ToDo
known bugs: no doublon management in exports/imports.