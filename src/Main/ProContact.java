package Main;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import Enums.Gender;
import Exceptions.ContactExceptions;

public class ProContact extends Contact implements java.io.Serializable {

	private static final long serialVersionUID = 750807424892349528L;
	private String mail;
	private String cieName;
	private LocalDate anciente;

	

	public ProContact(String firstName, String lastName, Enum<Gender> sexe, String mail, String cieName,
			String anciente) throws ContactExceptions {
		super(firstName, lastName, sexe);


		setMail(mail);
		setCieName(cieName);
		setAnciente(anciente);
	}

	public ProContact(Contact c, String mail, String cieName, String anciente) throws ContactExceptions {
		super(c);


		setMail(mail);
		setCieName(cieName);
		setAnciente(anciente);
	}

	public String getMail() {
		return mail;
	}

	public String getCieName() {
		return cieName;
	}

	public LocalDate getAnciente() {
		return anciente;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setCieName(String cieName) {
		this.cieName = cieName;
	}

	public void setAnciente(String anciente) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
		LocalDate localDate = LocalDate.parse(anciente, formatter);
		this.anciente = localDate;
	}

//	public String calcAncienete() {
////		getAnciente() 
//	}
	
	@Override
	public String toString() {
		LocalDate now = LocalDate.now();
		long diffMonths = ChronoUnit.MONTHS.between(getAnciente(), now);
		long mounths, years;
		if(diffMonths < 12) {
			mounths = diffMonths;
			years = 0;
		} else {
			years = Math.floorDiv(diffMonths, 12);
			mounths = diffMonths % 12;
		}
		return "\nContact Pro"
				+ "\n" + super.getLastName() + " " + super.getFirstName() 
				+ "\nMail: " + getMail()
				+ "\nEntre(e) dans la societe " + cieName + " depuis " + years + " an(s) et " + mounths +  " mois\n";
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
