package Main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import CLI.Menu;
import Enums.FamilySituation;
import Enums.Gender;
import Exceptions.ContactExceptions;

public class StartSmallProject {

	public static void main(String[] args) {
		List<Contact> contacts = new ArrayList<>();
		dirtyTesting(contacts);
		Scanner sc = new Scanner(System.in);
		new Menu(contacts, sc);
		sc.close();
		
	}

	static void dirtyTesting(List<Contact> contacts) {
		try {
			PrivateContact p = new PrivateContact("Nicolas", "Martino", Gender.M, "05061", FamilySituation.C, "10/04/1987");
			System.out.println(p.toString());
			contacts.add(p);
			ProContact proC1 = new ProContact("Jack", "Sparrow", Gender.M, "qsdfqsdf@qsdf.qsdf", "cieCorp","02/06/2015");
			System.out.println(proC1.toString());
			contacts.add(proC1);
			ProContact proC = new ProContact("Jack", "Chirac", Gender.M, "qsdfqsdf@qsdf.qsdf", "cieCorp","02/06/2015");
			System.out.println(proC.toString());
			contacts.add(proC);
			ProContact proC2 = new ProContact("Bernie", "Chirac", Gender.M, "qsdfqsdf@qsdf.qsdf", "cieCorp","02/06/2015");
			System.out.println(proC2.toString());
			contacts.add(proC2);

		} catch (ContactExceptions e) {
			e.printStackTrace();
		}
	}
}