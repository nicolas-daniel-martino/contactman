package Main;
import java.util.UUID;

import Enums.Erreurs;
import Enums.Gender;
import Exceptions.ContactExceptions;

public class Contact implements java.io.Serializable{ //!abstract => code is dry cf CLI.SubMenuCreate line 13

	private static final long serialVersionUID = 7549737155513190386L;
	private UUID id = UUID.randomUUID();
	private String firstName;
	private String lastName;
	private Enum<Gender> sexe;

	public Contact(String firstName, String lastName, Enum<Gender> sexe) throws ContactExceptions {
		setFirstName(firstName);
		setLastName(lastName);
		setSexe(sexe);
	}
	protected Contact(Contact c) throws ContactExceptions {
		setFirstName(c.getFirstName());
		setLastName(c.getLastName());
		setSexe(c.getSexe());
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setFirstName(String firstName) throws ContactExceptions {
		if (firstName.length() > 49)
			throw new ContactExceptions(Erreurs.Prenom_TROP_LONG);
		this.firstName = Character.toUpperCase(firstName.charAt(0)) + firstName.substring(1).toLowerCase();
	}

	public void setLastName(String lastName) throws ContactExceptions {
		if (lastName.length() > 49)
			throw new ContactExceptions(Erreurs.NOM_TROP_LONG);
		this.lastName = lastName.toUpperCase();
	}

	@Override
	public String toString() {
		return "Contact [firstName=" + firstName + ", lastName=" + lastName + ", sexe=" + sexe + "]";
	}

	public Enum<Gender> getSexe() {
		return sexe;
	}

	public void setSexe(Enum<Gender> sexe) {
		this.sexe = sexe;
	}
	public String getFullName() {
		return getFirstName() + getLastName();
	}
	public UUID getID() {
		return id;
	}
}
