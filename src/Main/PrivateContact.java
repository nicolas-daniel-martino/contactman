package Main;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import Enums.FamilySituation;
import Enums.Gender;
import Exceptions.ContactExceptions;

public class PrivateContact extends Contact implements java.io.Serializable {

	private static final long serialVersionUID = -1380641092962943667L;
	private String tel;
	private Enum<FamilySituation> familySituation;
	private LocalDate anniversaire;

	public PrivateContact(Contact c, String tel, Enum<FamilySituation> familySituation, String anniversaire)
			throws ContactExceptions {
		super(c);
		setTel(tel);
		setFamilySituation(familySituation);
		setAnniversaire(anniversaire);
	}

	public PrivateContact(String firstName, String lastName, Enum<Gender> sexe, String tel,
			Enum<FamilySituation> familySituation, String anniversaire) throws ContactExceptions {
		super(firstName, lastName, sexe);
		setTel(tel);
		setFamilySituation(familySituation);
		setAnniversaire(anniversaire);
		;
	}

	public String getTel() {
		return tel;
	}

	public Enum<FamilySituation> getFamilySituation() {
		return familySituation;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public void setFamilySituation(Enum<FamilySituation> familySituation) {
		this.familySituation = familySituation;
	}

	@Override
	public String toString() {
		LocalDate now = LocalDate.now();
		long diffDays = ChronoUnit.DAYS.between(getAnniversaire(), now);
		long diffYears = ChronoUnit.YEARS.between(getAnniversaire(), now);

		return "\nContact Perso"
			+ "\n" + super.getLastName() + " " + super.getFirstName() 
			+ "\nTelephone: " + getTel() 
			+ "\nAge: " + diffYears  +" ans"
			+ (diffDays == 0 ? " et Bon anniversaire!" : "");
	}

	public LocalDate getAnniversaire() {
		return anniversaire;
	}

	public void setAnniversaire(String anniversaire) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
		LocalDate localDate = LocalDate.parse(anniversaire, formatter);
		this.anniversaire = localDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
