package CLI;

import java.util.Scanner;

import Enums.FamilySituation;
import Enums.Gender;
import Exceptions.ContactExceptions;
import Main.Contact;
import Main.PrivateContact;
import Main.ProContact;

public class SubMenusCreate {
	/* Function summary
	 * Creates the different contact types
	 */
	public static Contact createContact(Scanner sc) { //keeps code dry
		sc.reset();
		System.out.println("Tapez le prenom du contact: ");
		String firstName = sc.nextLine();
		System.out.println("Tapez le nom du contact: ");
		String lastName = sc.nextLine();

		String sexe = FieldChecks.checkSex(sc);
		try {
			return new Contact(firstName, lastName, Gender.valueOf(sexe));
		} catch (ContactExceptions e) {
			System.out.println(e.getMessage());
			System.out.println("Veuillez recommencer votre saisie.");
			createContact(sc);
		}

		return null;
	}
	public static  String chooseContactType(Scanner sc) { // tzo option pro(P) and private(L)
		sc.reset();
		System.out.println("Ce contact sera proffesionnel(P) ou prive(L)");
		String response = sc.nextLine().toUpperCase();
		if (!(response.equals("P") || response.equals("L")))
			return chooseContactType(sc);
		return response;
	}
	public static ProContact createProContact(Contact c, Scanner sc) {
		sc.reset();
		System.out.println("Tapez le mail du contact: ");
		String mail = sc.nextLine();
		System.out.println("Tapez le nom de l'entreprise du contact: ");
		String cieName = sc.nextLine();

		try {
			System.out.println("Tapez la date d'arrivee dans l'entreprise (au format dd/MM/YYYY) du contact: ");
			String anciente = sc.nextLine();
			return new ProContact(c, mail, cieName, anciente);
		} catch (ContactExceptions e) {
			System.out.println(e.getMessage());
			System.out.println("Veuillez recommencer votre saisie.");
			createProContact(c, sc);
		} catch (NumberFormatException e) {
			System.out.println("Attention, l'age doit etre un nombre.");
			return createProContact(c, sc);
		} catch (Exception e) {
			System.out.println("Erreur inconnue, veuillez recommencer votre saisie.");
			return createProContact(c, sc);
		}
		return null;
	}

	public static PrivateContact createPrivateContact(Contact c, Scanner sc) {
		sc.reset();
		System.out.println("Tapez le numero de telephone du contact (ex: 00-00-00-00-00): ");
		String tel = sc.nextLine();

		String familySituation = FieldChecks.checkFamilySituation(sc);

		try {
			System.out.println("Tapez la data de naissance du contact (au format dd/MM/YYYY) : ");
			String anniversaire = sc.nextLine();
			return new PrivateContact(c, tel, FamilySituation.valueOf(familySituation), anniversaire);
		} catch (ContactExceptions e) {
			System.out.println(e.getMessage());
			System.out.println("Veuillez recommencer votre saisie.");
			return createPrivateContact(c, sc);
		}  catch (Exception e) {
			System.out.println("Erreur inconnue, veuillez recommencer votre saisie.");
			return createPrivateContact(c, sc);
		}
	}

}
