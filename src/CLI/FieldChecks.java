package CLI;

import java.util.Scanner;

public class FieldChecks {
	/* Function summary
	 * Checks Enum fields
	 */
	public static String checkSex(Scanner sc) {
		sc.reset();
		String sexe = "";
		Boolean isValidInput = false;
		while (!isValidInput) {
			System.out.println("Tapez le sexe (F/M) du contact:");
			sexe = sc.nextLine().toUpperCase();
			if (sexe.equals("F") || sexe.equals("M"))
				isValidInput = true;
			else
				System.out.println("Entree invalide " + sexe + " veuillez reessayer.");
		}
		return sexe;
	}

	public static String checkFamilySituation(Scanner sc) {
		sc.reset();
		String familySituation = "";
		System.out.println("Tapez la situation familiale (C: Celibataire, M:Marie, D: Divorce, X:Autre) du contact: ");
		familySituation = sc.nextLine().toUpperCase();
		if (familySituation.equals("C") || familySituation.equals("M") || familySituation.equals("D")
				|| familySituation.equals("X"))
			return familySituation;
		System.out.println("Entree invalide " + familySituation + " veuillez reessayer.");
		return checkFamilySituation(sc);
	}

}
