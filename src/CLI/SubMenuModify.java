package CLI;

import java.util.List;
import java.util.Scanner;

import Enums.FamilySituation;
import Enums.Gender;
import Exceptions.ContactExceptions;
import Main.Contact;
import Main.PrivateContact;
import Main.ProContact;

public class SubMenuModify extends Menu{
	/* Function summary
	 * Modifies the different contact types
	 */
	public static Boolean modifierUnContact(Scanner sc, List<Contact> contacts) {
		sc.reset();
		System.out.println(
				"Tapez le numero du contact a modifier ou (L) pour afficher la liste des contacts ou pour quitter tapez (Q)");
		String response = sc.nextLine().toUpperCase();
		if (response.equals("L")) {
			ListerLesContact(contacts);
			return modifierUnContact(sc, contacts);
		}
		if (response.equals("Q")) {
			return false;
		}
		try {
			int contactnumber = Integer.parseInt(response);
			Contact selectedContact = contacts.get(contactnumber);
			System.out.println("Modifier la fiche contact: \n" + selectedContact.toString());

			if (selectedContact instanceof PrivateContact) {
				modifierUnContactPrive(sc, (PrivateContact) selectedContact);
				return true;
			}
			if (selectedContact instanceof ProContact) {
				modifierUnContactPro(sc, (ProContact) selectedContact);
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return false;

	}

	public static void modifierUnContactPrive(Scanner sc, PrivateContact c) throws ContactExceptions {
		System.out.println(
				"Vous souhaitez modifier le nom(N), le prenom (p), le sexe(S), la date d'lanniversaire (A), le numero de telephone (T) ou la situation familiale (SF) ");
		String response = sc.nextLine().toUpperCase();
		switch (response) {
		case "N":
			System.out.println("Modifier le nom du contact " + c.getLastName() + " en :");
			c.setLastName(sc.nextLine());
			break;
		case "P":
			System.out.println("Modifier le prenom du contact " + c.getFirstName() + " en :");
			c.setFirstName(sc.nextLine());
			break;
		case "S":
			System.out.println("Modifier le sexe du contact " + c.getSexe());
			c.setSexe(Gender.valueOf(FieldChecks.checkSex(sc)));
			break;
		case "A":
			System.out.println("Modifier la date de naissance du contact (au format dd/MM/YYYY) en :");
			c.setAnniversaire(sc.nextLine());
			break;
		case "T":
			System.out.println("Modifier le numero de telephone du contact " + c.getTel() + " en :");
			c.setTel(sc.nextLine());
			break;
		case "SF":
			System.out.println("Modifier la situation familiale du contact " + c.getFamilySituation() + " en :");
			c.setFamilySituation(FamilySituation.valueOf(FieldChecks.checkFamilySituation(sc)));
		default:
			break;
		}
		System.out.println("Le contact: " + c.toString());
	}

	public static void modifierUnContactPro(Scanner sc, ProContact c) throws ContactExceptions {
		System.out.println(
				"Vous souhaitez modifier le nom(N), le prenom (p), le sexe(S), le mail (M), le nom de l'entreprise (E) ou l'ancienet� (A) ");
		String response = sc.nextLine().toUpperCase();
		switch (response) {
		case "N":
			System.out.println("Modifier le nom du contact " + c.getLastName() + " en :");
			c.setLastName(sc.nextLine());
			break;
		case "P":
			System.out.println("Modifier le prenom du contact " + c.getFirstName() + " en :");
			c.setFirstName(sc.nextLine());
			break;
		case "S":
			System.out.println("Modifier le sexe du contact " + c.getSexe());
			c.setFirstName(FieldChecks.checkSex(sc));
			break;
		case "M":
			System.out.println("Modifier le mail du contact " + c.getMail() + " en :");
			c.setMail(sc.nextLine());
			break;
		case "E":
			System.out.println("Modifier l'entreprise du contact " + c.getCieName() + " en :");
			c.setCieName(sc.nextLine());
			break;
		case "A":
			System.out.println("Modifier la date d'arrivee du contact (au format dd/MM/YYYY) en :");
			c.setAnciente(sc.nextLine());
			break;

		default:
			break;
		}
		System.out.println("Le contact: " + c.toString());
	}
}
