package CLI;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import Main.*;

public class Menu { 
/*
 * Function summary: Main cli menu, List sorted contacts, export/import contacts, delete contact
 * Launches Submenus to create/modify/search and check fields
 */
	public Menu() {
	}

	public Menu(List<Contact> contacts, Scanner sc) {
		System.out.println("\n------------------\n" + "Bienvenue: \n------------------"
				+ "\nCe programme est destine a la gestion de contacts");
		String res;
		do {

			System.out.println("\n------------------\n" + "Menu: \n------------------"
					+ "\nPour lister l'ensemble des contacts tapez (L)" + "\nPour ajouter un contact tapez (A)"
					+ "\nPour modifier un contact tapez (M)" + "\nPour supprimer un contact tapez (S)"
					+ "\nPour effectuer une recherche par nom d'un contact tapez (R)"
					+ "\nPour effectuer une recherche sur les noms complets de vos contacts tapez (F)"
					+ "\nPour effectuer exporter vos contacts (E)" + "\nPour importer vos contacts (I)"
					+ "\nPour quitter tapez (Q).");
			res = sc.nextLine().toUpperCase();

			switch (res) {
			case "A":
				Contact c = SubMenusCreate.createContact(sc);
				if (c == null) {
					System.out.println("Une erreur inconnue a interrompu votre saisie");
					break;
				}

				switch (SubMenusCreate.chooseContactType(sc)) {
				// choose pro
				case "P":
					ProContact procontact = SubMenusCreate.createProContact(c, sc);
					if (procontact == null) {
						System.out.println("Une erreur inconnue a interrompu votre saisie");
						break;
					}
					contacts.add(procontact);
					System.out.println("L'ajout a �t� un succ�s!\n" + procontact.toString());
					System.out.println("Saisissez (Entr�) pour continuer...");
					sc.nextLine();
					break;
				case "L": // chose private
					PrivateContact privatecontact = SubMenusCreate.createPrivateContact(c, sc);
					if (privatecontact == null) {
						System.out.println("Une erreur inconnue a interrompu votre saisie");
						break;
					}
					contacts.add(privatecontact);
					System.out.println("L'ajout a �t� un succ�s!\n" + privatecontact.toString());
					System.out.println("Saisissez (Entr�) pour continuer...");
					sc.nextLine();
					break;
				default:
					break;
				}

				break;
			// CMD afficher la liste des contacts
			case "L":
				ListerLesContact(contacts);
				break;
			// cmd Modifier
			case "M":
				if (!(contacts.size() > 0)) {
					System.out.println("Pas de contact � modifier !");
					break;
				}
				Boolean succes = SubMenuModify.modifierUnContact(sc, contacts);
				if (succes)
					System.out.println("A �t� modifi� avec succ�s!");
				else
					System.out.println("N'a pas �t� modifi� avec succ�s!");
				break;
			case "S":
				if (!(contacts.size() > 0)) {
					System.out.println("Pas de contact � modifier !");
					break;
				}
				supprimerUnContact(sc, contacts);
				break;
			case "F":
				Search.byKeywords(contacts, sc);
				break;
			case "E":
				exporterDesContact(contacts);
				break;
			case "I":

				importerDesContact(contacts);
				break;
			default:
				break;
			}

		} while (!res.equals("Q"));
		sc.close();
	}

	public static void ListerLesContact(List<Contact> contacts) {

		if (contacts.size() > 0) {
			List<Contact> result = new ArrayList<Contact>();
			result = contacts.stream()
					.sorted(Comparator.comparing(Contact::getLastName).thenComparing(Contact::getFirstName))
					.collect(Collectors.toList());
			result.forEach(contact -> System.out.println(contact.toString()));
		} else
			System.out.println("No contact found!");
	}

	public static void exporterDesContact(List<Contact> contacts) {
		//TODO AD namecheck on files to avoid multiple export
		for (Contact contact : contacts) {
			try {
				String filename = "./contact/" + contact.getLastName() + "_" + contact.getFirstName() + "_"
						+ contact.getID().toString() + ".ser";
				FileOutputStream fileOut = new FileOutputStream(filename);
				ObjectOutputStream out = new ObjectOutputStream(fileOut);
				out.writeObject(contact);
				out.close();
				fileOut.close();
				System.out.printf("\nSerialized data is saved in ./contact/" + filename);
			} catch (IOException i) {
				i.printStackTrace();
			}
		}

	}

	public static void importerDesContact(List<Contact> contacts) {

		try (Stream<Path> paths = Files.walk(Paths.get("./contact"))) {
			paths.filter(Files::isRegularFile).forEach(contactPath -> {
				Contact contact = null;
				try {
					FileInputStream fileIn = new FileInputStream(contactPath.toString());
					ObjectInputStream in = new ObjectInputStream(fileIn);
					contact = (Contact) in.readObject();
					System.out.println("The following contact was imported: i" + contact.toString());
					contacts.add(contact);
					in.close();
					fileIn.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
							});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static Boolean supprimerUnContact(Scanner sc, List<Contact> contacts) {
		sc.reset();
		System.out.println(
				"Tapez le numero du contact a supprimer ou (L) pour afficher la liste des contacts ou pour quitter tapez (Q)");
		String response = sc.nextLine().toUpperCase();
		if (response.equals("L")) {
			ListerLesContact(contacts);
			return supprimerUnContact(sc, contacts);
		}
		if (response.equals("Q")) {
			return false;
		}
		try {
			int contactnumber = Integer.parseInt(response);
			Contact selectedContact = contacts.get(contactnumber);
			System.out.println("Supprimer la fiche contact: \n" + selectedContact.toString());
			System.out.println("Etes-vous sur de vouloir supprimer ce contact (Y/N)");
			String confirm = sc.nextLine().toUpperCase();

			if (confirm.equals("Y")) {
				contacts.remove(contactnumber);
				System.err.println("qsdfqsdf");
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return false;
	}

}
