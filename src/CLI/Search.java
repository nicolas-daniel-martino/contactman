package CLI;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Main.Contact;

public class Search {
	/* Function summary
	 * Regex single term search
	 */
	public static void byKeywords(List<Contact> contacts, Scanner sc) {
		System.out.println("Tapez le prenom ou le nom du contact");
		String response = sc.nextLine();
		for (int i = 0; i < contacts.size(); i++) {
			String txt = contacts.get(i).getFullName();

			Pattern p = Pattern.compile(response, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			Matcher m = p.matcher(txt);
			if (m.find()) {
				System.out.print("Entree numero: " + i + contacts.get(i).toString());
			}
		}

	}
}
