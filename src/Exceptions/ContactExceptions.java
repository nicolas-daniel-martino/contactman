package Exceptions;

import Enums.Erreurs;

public class ContactExceptions extends Exception{
	static final long serialVersionUID = 1L;
	private Erreurs err;

	private String message;

	public ContactExceptions(Erreurs err) {
		this.err = err;
	}

	@Override
	public String getMessage() {
		message = "Erreur: ";
		switch (err) {
		case NOM_TROP_LONG:
			message += "Votre nom contient plus de 50 caract�re, veuillez entrer un nom plus court";
			break;
		case Prenom_TROP_LONG:
			message += "Votre prenom contient plus de 50 caract�re, veuillez entrer un prenom plus court";
			break;	
		default:
			message += ": Autre type d'�rreur!";
			break;
		}
		return message;
	}

}
